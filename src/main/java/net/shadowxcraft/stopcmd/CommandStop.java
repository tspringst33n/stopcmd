package net.shadowxcraft.stopcmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandStop implements CommandExecutor {

	private Main plugin;

	public CommandStop(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		// -------------------------------------------- //
		// Config variables. Easier to fix...
		int y = plugin.config.delayTime();
		int x = y * 100;
		String message = plugin.config.shutdownMessage();
		Boolean enabled = plugin.config.getEnabled();
		String command = plugin.config.commandToIssue();
		// -------------------------------------------- //
		if (sender.hasPermission("minecraft.command.stop")) {
			if (enabled == true) {
				plugin.getServer().broadcastMessage(message);

				try {
					Thread.sleep(x); // Sleeps for X time
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				plugin.getServer().dispatchCommand(
						plugin.getServer().getConsoleSender(), command);

				try {
					Thread.sleep(x); // Sleeps for X time
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				plugin.getServer().shutdown();
				return true;
			} else {
				plugin.getServer()
						.broadcastMessage(
								"Plugin is disabled. Check /plugins/StopCMD/config.yml");
				return true;
			}
		} else {
			sender.sendMessage(ChatColor.RED + "No permissions!");
			return true;
		}
	}
}
