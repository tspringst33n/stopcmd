package net.shadowxcraft.stopcmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandToggle implements CommandExecutor {
	private Main plugin;

	public CommandToggle(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender.hasPermission("minecraft.command.stop")) {
			plugin.config.ToggleEnabled();
			return true;
		} else {
			sender.sendMessage(ChatColor.RED + "No permissions!");
			return true;
		}
	}
}
