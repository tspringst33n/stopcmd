package net.shadowxcraft.stopcmd;

import net.shadowxcraft.stopcmd.config.Config;

import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

	public Plugin plugin() {
		Main instance = this;
		return instance;

	}

	public Config config;

	@Override
	public void onEnable() {

		this.config = new Config(this); // defines config
		setupConfig();

		// -------------------------------------------- //
		// Commands
		// -------------------------------------------- //
		this.getCommand("stop").setExecutor(new CommandStop(this));
		this.getCommand("toggle").setExecutor(new CommandToggle(this));
		
		// -------------------------------------------- //
		// Config version check
		// -------------------------------------------- //
		config.ConfigUpdate();

	}

	@Override
	public void onDisable() {

	}

	// -------------------------------------------- //
	// Setup Configuration Files.
	// -------------------------------------------- //
	public void setupConfig() {

		Config conf = new Config(this);
		conf.LoadConfig();
	}
}
