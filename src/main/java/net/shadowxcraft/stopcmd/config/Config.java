package net.shadowxcraft.stopcmd.config;

import java.io.File;
import java.io.IOException;

import net.shadowxcraft.stopcmd.Main;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
	private Main plugin;
	private FileConfiguration config;
	
	private static final double CURRENT_VERSION = 1.1;

	public Config(Main plugin) {
		this.plugin = plugin;
		LoadConfig();
	}

	public boolean Loaded() {
		return this.config != null;
	}

	public void Reload() {
		LoadConfig();
	}

	public boolean LoadConfig() {
		File file = new File(this.plugin.getDataFolder(), "config.yml");
		if (!file.exists()) {
			NewConfig(file);
		} else {
			try {
				this.config = YamlConfiguration.loadConfiguration(file);
				
			} catch (Exception e) {
				e.printStackTrace();
				this.config = null;
			}
		}
		return this.config != null;
	}

	private void NewConfig(File file) {
		this.config = new YamlConfiguration();
		//this.config.createSection("#The Enabled option sets if the plugin is enabled or not.");
		this.config.set("Enabled", Boolean.valueOf(false));
		this.config.set("OnShutdown.ShutdownMessage", "Server is shutting down. Sending to secondary server.");
		this.config.set("OnShutdown.DelayTime", 10);
		this.config.set("OnShutdown.CommandToIssue", "kickall");
		this.config.set("Version", CURRENT_VERSION);
		try {
			this.config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
			this.config = null;
		}
	}
	
	public void ConfigUpdate(){
		File file = new File(this.plugin.getDataFolder(), "config.yml");
		File fileOLD = new File(this.plugin.getDataFolder(), "configOLD.yml");
		Double ConfVersion =  this.config.getDouble("Version");
		if(ConfVersion == CURRENT_VERSION){
			plugin.getLogger().info(ChatColor.GOLD + "StopCMD Config.yml version matches. No update needed");
			return;
		}
		if(ConfVersion != CURRENT_VERSION){
			plugin.getLogger().info(ChatColor.RED + "StopCMD Config.yml version does not match!. Updating config to latest version. The old config has been named configOLD.yml");
			file.renameTo(fileOLD);
			try {
				this.config.save(fileOLD);
			} catch (IOException e) {
				e.printStackTrace();
				this.config = null;
			}
			
			RegenConfig();
			return;
		}else{
			plugin.getLogger().info(ChatColor.RED + "Unknown Error occured. Try restarting the server. If this message keeps appearing, contact the developer Tspringst33n");
			return;
		}
		
		
	}
	
	public void ToggleEnabled(){
		File file = new File(this.plugin.getDataFolder(), "config.yml");
		if(plugin.config.getEnabled() == false){
			this.config.set("Enabled", Boolean.valueOf(true));	
			plugin.getServer().broadcastMessage("StopCMD has been enabled! Please check the config if you have not done so before.");
			try {
				this.config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
				this.config = null;
			}return;
		}
		if(plugin.config.getEnabled() == true){
			this.config.set("Enabled", Boolean.valueOf(false));
			plugin.getServer().broadcastMessage("StopCMD has been disabled!");
			try {
				this.config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
				this.config = null;
			}return;		
		}	
		return;
	}
	
	public void RegenConfig() {
		File file = new File(this.plugin.getDataFolder(), "config.yml");
		file.delete();
		NewConfig(file);
	}
	public boolean getEnabled(){
		return this.config.getBoolean("Enabled");
	}	
	
	public int delayTime(){
		return this.config.getInt("OnShutdown.DelayTime");
	}	
	
	public String commandToIssue(){
		return this.config.getString("OnShutdown.CommandToIssue");
	}	
	
	public String shutdownMessage(){
		return this.config.getString("OnShutdown.ShutdownMessage");
	}	
}